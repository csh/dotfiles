;;; kickstart.scm - My desktop configuration.
;;; Copyright (C) 2025  jgart <jgart@dismail.de>
;;; Copyright (C) 2025  Caleb S. Herbert <csh@bluehome.net>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (kickstart)
  #:use-module (gnu home services)
  #:use-module (gnu home services guix)
  #:use-module (gnu packages)
  #:use-module (gnu packages fonts)
  #:use-module (gnu services)

  #:use-module (guix channels)

  #:use-module (rde features base)
  #:use-module (rde features emacs)
  #:use-module (rde features emacs-xyz)
  #:use-module (rde features fontutils)
  #:use-module (rde features gnupg)
  #:use-module (rde features guile)
  #:use-module (rde features image-viewers)
  #:use-module (rde features keyboard)
  #:use-module (rde features linux)
  #:use-module (rde features mail)
  #:use-module (rde features markup)
  #:use-module (rde features matrix)
  #:use-module (rde features networking)
  #:use-module (rde features password-utils)
  #:use-module (rde features shells)
  #:use-module (rde features ssh)
  #:use-module (rde features terminals)
  #:use-module (rde features tmux)
  #:use-module (rde features version-control)
  #:use-module (rde features video)
  #:use-module (rde features web-browsers)
  #:use-module (rde features wm)
  #:use-module (rde features xdg)
  #:use-module (rde features)
  #:use-module (rde packages fonts))

(rde-config-home-environment
 (rde-config
  (features
   (list
    (feature-user-info
     #:user-name "caleb"
     #:full-name "Caleb Herbert"
     #:email "csh@bluehome.net"
     #:emacs-advanced-user? #f)

    ;; Emacs
    (feature-emacs-webpaste)
    (feature-emacs-appearance)
    (feature-emacs-modus-themes)
    (feature-emacs-all-the-icons)
    (feature-emacs-completion)
    (feature-emacs
     #:default-terminal? #f
     #:default-application-launcher? #f
     #:extra-init-el
     '((global-page-break-lines-mode 1)
       (setq eradio-player '("mpv" "--no-video" "--no-terminal"))
       (global-set-key (kbd "C-c r p") 'eradio-play)
       (global-set-key (kbd "C-c r s") 'eradio-stop)
       (global-set-key (kbd "C-c r t") 'eradio-toggle)
       (setq
        eradio-channels
        '(("94.9 KCMO" . "https://provisioning.streamtheworld.com/pls/KCMOFMAAC.pls")
          ("KQRC 98.9 The Rock!" . "https://live.amperwave.net/manifest/audacy-kqrcfmaac-hlsc.m3u8")
          ("101 The Fox" . "https://playerservices.streamtheworld.com/api/livestream-redirect/KCFXFMAAC.aac")
          ("KKFI 90.1 Kansas City, MO" . "http://pacificaservice.org:8000/kkfi_64?type=.mp3")
          ("iHeart2000s" . "https://stream.revma.ihrhls.com/zc6850/hls.m3u8")
          ("aNONradio" . "http://anonradio.net:8000/anonradio")
          ("Chabad.org Jewish Music" . "https://stream.radio.co/sdfd68a101/listen")
          ("Chabad.org Torah Classes Radio" . "https://streams.radio.co/s9f35c3afc/listen")
          ("KZHE Classic Country" . "http://crystalout.surfernetwork.com:8001/KZHE-FM_MP3")))
       (setq elfeed-feeds
             '("https://trisquel.info/en/node/feed"
               "https://trisquel.info/en/taxonomy/term/700/0/feed"
               "https://trisquel.info/en/taxonomy/term/50/0/feed"
               "https://www.reddit.com/r/trisquel/.rss"
               "https://www.gnu.org/rss/whatsnew.rss"
               "https://static.fsf.org/fsforg/rss/news.xml"
               "https://static.fsf.org/fsforg/rss/events.xml"
               "https://static.fsf.org/fsforg/rss/blogs.xml"
               "https://www.reddit.com/r/gnu/.rss"
               "https://www.wingolog.org/feed/atom"
               "https://dthompson.us/feed.xml"
               "https://spritely.institute/feed.xml"
               "https://stallman.org/rss/rss.xml"
               "https://jxself.org/rss20.xml"
               "https://www.freakingpenguin.com/feed.xml"
               "https://systemcrafters.net/rss/news.xml"
               "file:///home/caleb/Downloads/qN2nbKGW"
               "https://ebb.org/bkuhn/blog/rss.xml"
               "https://hack.org/mc/blog/index.xml"
               "https://boehs.org/in/blog.xml"
               "https://stallmansupport.org/feed.xml"
               "https://personaljournal.ca/paulsutton/feed/"
               "https://rosenzweig.io/feed.xml"
               "https://tylerdback.com/index.xml"
               "http://www.xahlee.info/emacs/emacs/blog.xml"
               "https://sachachua.com/blog/feed/"
               "https://www.snamellit.com/blog/keepassxc-and-flatpak/rss.xml"
               "https://theluddite.org/feed.rss"
               "https://glenneth.srht.site/feed.xml"
               "https://faif.us/feeds/cast-ogg/"))
       ))

    ;; Desktop
    (feature-pipewire)
    (feature-backlight)
    (feature-waybar)
    (feature-desktop-services)
    (feature-networking)
    (feature-fonts ; Recommendations from Butterick's Practical Typography.
     #:font-monospace (font
                       (name "Adobe Source Code Pro")
                       (size 11)
                       (package font-adobe-source-code-pro))
     #:font-serif (font
                   (name "Charter")
                   (size 11)
                   (package font-charter))
     #:font-sans (font
                  (name "IBM Plex Sans")
                  (size 11)
                  (package font-ibm-plex))
     #:font-unicode (font
                     (name "Noto Color Emoji")
                     (size 11)
                     (package font-noto-color-emoji)))
    (feature-sway)
    (feature-swayidle)
    (feature-sway-screenshot)
    (feature-swaynotificationcenter)
    (feature-kanshi)
    (feature-batsignal)
    (feature-swaylock)
    (feature-keyboard
     #:keyboard-layout
     (keyboard-layout "gb,il,ru" "dvorak,,"
                      #:options '("grp:win_space_toggle" "ctrl:nocaps")))
    (feature-imv)
    (feature-mpv)
    (feature-librewolf)
    (feature-xdg
     #:xdg-user-directories-configuration
     (home-xdg-user-directories-configuration
      (music "$HOME/Music")
      (videos "$HOME/Videos")
      (pictures "$HOME/Pictures")
      (documents "$HOME/Documents")
      (download "$HOME/Downloads")
      (desktop "$HOME")
      (publicshare "$HOME")
      (templates "$HOME")))

    ;; Terminal
    (feature-foot)
    (feature-bash)
    (feature-zsh)
    (feature-tmux)
    (feature-emacs-shell)
    (feature-vterm)
    (feature-ssh)
    (feature-gnupg
     #:gpg-primary-key "631CC434A56B5CBDFF21234697643795FA3E4BCE"
     #:ssh-keys '(("DE84748EB625295D3DA758528B0AAA3568A35C3C"))
     #:gpg-ssh-agent? #t)
    (feature-password-store #:password-store-directory "$HOME/.password-store")
    (feature-git)

    ;; Mail
    (feature-mail-settings
     #:mail-accounts
     (list (mail-account
            (id 'personal)
            (type 'gandi) ; TODO: Create type bluehome.
            (fqda "csh@bluehome.net")
            (pass-cmd "pass show Email/bluehome.net"))))
    (feature-isync
     #:isync-serializers
     (acons 'bluehome (generate-isync-serializer
                       "bluehome.net"
                       (@@ (rde features mail providers) generic-folder-mapping)
                       #:cipher-string 'DEFAULT@SECLEVEL=1
                       #:pipeline-depth 1)
            %default-isync-serializers))
    (feature-notmuch)
    (feature-msmtp
     #:msmtp-provider-settings
     (cons*
      `(bluehome . ((host . "bluehome.net")
                   (port . 587)))
      %default-msmtp-provider-settings))


    ;; Programming
    (feature-tex)
    (feature-markdown)
    (feature-guile)

    ;; Matrix - Currently does not work.
    (feature-matrix-settings
     #:homeserver "https://chat.fedoraproject.org"
     #:matrix-accounts
     (list (matrix-account
            (id "@ke0vvt:chat.fedoraproject.org")
            (server "chat.fedoraproject.org"))))
    (feature-emacs-ement)

    (feature-custom-services
     #:home-services
     (list (simple-service
            'variant-packages-service
            home-channels-service-type
            (list
             (channel
              (name 'kolev)
              (url "https://codeberg.org/csh/guix-channel")
              (introduction
               (make-channel-introduction
                "73627aaf61f33f6b9c093d9256c539e9a1ba1761"
                (openpgp-fingerprint
                 "631C C434 A56B 5CBD FF21  2346 9764 3795 FA3E 4BCE"))))
             (channel
              (name 'nonguix)
              (url "https://gitlab.com/nonguix/nonguix")
              ;; Enable signature verification:
              (introduction
               (make-channel-introduction
                "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                (openpgp-fingerprint
                 "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
             (channel
              (name 'rde)
              (url "https://git.sr.ht/~abcdw/rde")
              (introduction
               (make-channel-introduction
                "257cebd587b66e4d865b3537a9a88cccd7107c95"
                (openpgp-fingerprint
                 "2841 9AC6 5038 7440 C7E9  2FFA 2208 D209 58C1 DEB0"))))))))

    ;; Extra Packages
    (feature-base-packages
     #:home-packages
     (map specification->package
	  (list
           ;; Apps
           "dino"
           "gimp"
	   "torbrowser"
	   "vorta" "borg"
	   "zathura" "zathura-pdf-mupdf"

	   ;; Fedora Sway Spin
	   "light"
           "rofi" "dunst" "kanshi" "thunar"

           ;; Emacs packages
           "emacs-eradio"
           "emacs-elfeed"
           "emacs-page-break-lines"
           "emacs-yaml-mode"
           "emacs-vcard-mode"
           "emacs-csv-mode"
           "emacs-json-mode"
           "emacs-fountain-mode"
           "emacs-wisp-mode"
           "emacs-lilypond-mode"
           "emacs-robots-txt-mode"
           "emacs-olivetti"
           "emacs-ledger-mode"
           "emacs-paredit"
           "emacs-nov-el"
           "emacs-elpher"
           "emacs-gemini"

	   ;; Other Utilities
           "supdup" ; requires channel kolev.
           "jq"
           "xdg-desktop-portal-wlr"
	   "adwaita-icon-theme"
	   "cryptsetup"
	   "figlet"
	   "htop"
	   "octave"
	   "pavucontrol"
	   "r"
	   "slurp"
	   "tree"
	   "udiskie"
	   "xdg-utils"
	   "yt-dlp"

	   ;; OpenBSD base
	   "bc"
	   "bind"
	   "bsd-games"
	   "cpio"
	   "cups"
	   "cvs"
	   "dash"
	   "ed"
	   "m4"
	   "mailutils"
	   "ncompress"
	   "ncurses"
	   "oksh"
	   "opendoas"
	   "openldap"
	   "perl"
	   "rcs"
	   "rsync"
	   "sharutils"
	   "shell-functools"
	   "signify"
	   "tcsh"
	   "tmux"
	   "ytalk"
	   "zutils"
					;"finger"
					;#pax"
           )))))))
