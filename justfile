channel_git_keyring := "origin/keyring"
channel_intro_commit := "751dff0709e7154551cface225fb95f90423426a"
channel_intro_signer := "3B1D 7F19 E36B B60C 0F5B  2CA9 A52A A2B4 77B6 DD35"
email := "jgart@dismail.de"

alias a := auth
alias b := build
alias r := reconfigure

default:
    @just --list

reconfigure:
    ./pre-inst-env guix home reconfigure kickstart.scm

build:
    ./pre-inst-env guix home build kickstart.scm

auth:
    guix git authenticate --keyring={{channel_git_keyring}} \
                          --cache-key=channels/guix --stats "{{channel_intro_commit}}" "{{channel_intro_signer}}"
