![Screenshot](screenshot.png)

# Caleb's Dotfiles

This is my [`rde`](https://git.sr.ht/~abcdw/rde) config.

## Usage

Clone this repository and run the following command:

``` shell
just reconfigure
```

A manifest is provided with [`just`](https://toys.whereis.social/?search=just).

## Communication

[Write](mailto:csh@bluehome.net) to me if you have any questions about
my configuration.
