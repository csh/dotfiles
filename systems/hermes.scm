;; hermes.scm: ThinkPad X200s laptop Guix System config
;;
;; Copyright (C) 2024 Caleb S. Herbert <csh@bluehome.net>
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(use-modules (gnu) (gnu system))
(use-service-modules base cups dbus desktop networking sound ssh xorg)
(use-package-modules linux admin certs glib screen ssh video wm)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/Chicago")
  (keyboard-layout (keyboard-layout "gb"))
  (host-name "hermes")
  (issue "This is the GNU system.  Welcome.\nKernel \\r on an \\m (\\l)\n\n")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "caleb")
                  (comment "Caleb Herbert")
                  (group "users")
                  (home-directory "/home/caleb")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (list (specification->package "swaybg")
			  (specification->package "sway")
			  (specification->package "swaylock")
			  (specification->package "bemenu")
			  (specification->package "foot")
			  (specification->package "mpv")
                          (specification->package "swayidle"))
	    %base-packages))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (cons*
    (udev-rules-service 'pipewire pipewire)
    (service alsa-service-type
	     (alsa-configuration
	      (pulseaudio? #t)))
    (service cups-service-type)
    (service tor-service-type)   
    (service dbus-root-service-type)
    (service elogind-service-type)
    (service udisks-service-type)
    (service openssh-service-type)
    (service ntp-service-type
	     (ntp-configuration
	      (servers (map (lambda (server)
			      (ntp-server (address server)))
			    '("0.it.pool.ntp.org"
			      "1.it.pool.ntp.org"
			      "2.it.pool.ntp.org"
			      "3.it.pool.ntp.org")))))
    (service polkit-service-type)
    (service screen-locker-service-type
	     (screen-locker-configuration
	      (name "swaylock")
	      (program (file-append swaylock-effects "/bin/swaylock"))
	      (using-pam? #t)
	      (using-setuid? #f)))
    (service network-manager-service-type)
    (service wpa-supplicant-service-type)
    (modify-services %base-services
		     (mingetty-service-type config =>
					    (mingetty-configuration
					     (inherit config)
					     ;; Automatically log in as "caleb".
					     (auto-login "caleb")
					     ;; Work-around for "Error in service module" bug
					     (login-pause? #t)))))) ;; press enter to login

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss)

  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (targets (list "/dev/sda"))
                (keyboard-layout keyboard-layout)
		(terminal-outputs '(console))))
  (mapped-devices (list (mapped-device
                          (source (uuid
                                   "dd6f01be-339f-4628-b433-22294b6088f3"))
                          (target "cryptroot")
                          (type luks-device-mapping))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device "/dev/mapper/cryptroot")
                         (type "ext4")
                         (dependencies mapped-devices)) %base-file-systems)))
