;;; farnsworth.scm - Server config
;;; Copyright 2024 Caleb Herbert <csh@bluehome.net>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(use-modules (gnu))
(use-service-modules cups desktop networking ssh xorg docker avahi web certbot
		     dbus)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/Chicago")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "farnsworth")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "caleb")
                  (comment "Caleb Herbert")
                  (group "users")
                  (home-directory "/home/caleb")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))

  (packages (append (list (specification->package "tmux"))
                    %base-packages))

  (services
   (append (list
	    (service containerd-service-type)
	    (service docker-service-type)
	    (service elogind-service-type)
	    (service oci-container-service-type
		     (list
		      (oci-container-configuration
		       (image "jellyfin/jellyfin")
		       (provision "jellyfin")
		       (network "host")
		       (ports
			'(("8096" . "8096")))
		       (volumes
			'("jellyfin-config:/config"
			  "jellyfin-cache:/cache"
			  "/home/caleb/Media:/media")))))
	    (service oci-container-service-type
		     (list
		      (oci-container-configuration
		       (image "nextcloud/all-in-one")
		       (provision "nextcloud-aio-mastercontainer")
		       (network "host")
		       (ports '(("80" . "80")
				("8080" . "8080")
				("8443" . "8443")))
		       (volumes
			'("nextcloud_aio_mastercontainer:/mnt/docker-aio-config"
			  "/var/run/docker.sock:/var/run/docker.sock:ro")))))	    

            (service openssh-service-type)
	    (service avahi-service-type)
            (service tor-service-type)
            (service network-manager-service-type)
            (service wpa-supplicant-service-type)
            (service ntp-service-type))

           %base-services))
  (name-service-switch %mdns-host-lookup-nss)  ; mDNS support

  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (targets (list "/dev/sda"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "f0fb1e54-1ebe-491f-85ba-d4d3100371e5")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "16681867-4f5c-48d7-9513-0d2e7f1db7a4"
                                  'ext4))
                         (type "ext4"))
		       %base-file-systems)))
